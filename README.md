# ccao-pdf-extraction

Extract and redact tax forms for the Cook County Assessor's Office.

## Requirements

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## AWS Credentials

To run these scripts you'll need an IAM User and access keys. To create these credentials and set them on your local system, follow **steps 2 and 3** in [this Textract documentation](https://docs.aws.amazon.com/textract/latest/dg/setup-awscli-sdk.html) (you don't need to install the packages described in step 1). This repo expects to find AWS credentials in the directory `~/.aws`.

## Processing files locally

1. Clone this repository and cd into your local copy:

  ```bash
  git@github.com:datamade/ccao-pdf-extraction.git
  cd ccao-pdf-extraction
  ```

2. Add any PDFs you'd like to process to the `raw` directory. create a fodler called `original_raw_files` inside of `raw`
   `original_raw_files`  will contain all the original pdf files.
   Note that Make is very fussy about filenames, and while we've made sure to handle whitespace/special characters/signs
   in file names, you should take care to preprocess filenames and remove
   special characters, particularly dots, colons, and semicolons. Here are two method to do that:

- method 1: run python sript (recommanded)
```python
  python "<path>/ccao-pdf-extraction/scripts/preprocessing/clean_rawfilename.py"

```

- method 2: run bash command
```bash
for file in raw/original_raw_files/*.pdf; do
    mv "$file" $(echo "$file" | sed "s/\.//g" | sed -e "s/pdf$/.pdf/g" -e "s/PDF$/.pdf/g" | sed -e "s/[,;]//g" | sed -e "s/ /_/g");
done
```

3. Build your Docker container:

  ```bash
  docker-compose build
  ```

4. Run the Makefile:

  ```bash
  docker-compose run --rm make
  ```

The scripts will output PDFs and entity lists into one of two folders,
`final/form_8825` and `final/form_schedule_e`, depending on the detected entity type
of the raw PDF. If the scripts cannot detect a valid form 8825 or form schedule E
in a raw PDF, they will instead copy the **raw, unredacted PDF** to the
`final/form_unknown` folder.

## QA the result from final folder

Run QA python code 

```python
  python "<path>/ccao-pdf-extraction/scripts/QA/integrity_check.py"

```


## Cleaning up

To delete your processed files (in order to rebuild them, for example), run:

  ```bash
  docker-compose run --rm make clean
  ```
