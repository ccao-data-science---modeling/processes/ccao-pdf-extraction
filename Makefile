SHELL := /bin/bash

# Gracefully handle raw PDF names with spaces in them
PDF_RAW=$(shell find raw/ -iname '*.pdf' | sed 's/ /\\ /g')
PDF_FINAL=$(patsubst raw/%, process/%, $(PDF_RAW))

.PHONY: all clean process/%.pdf

all: $(PDF_FINAL)

clean:
	rm -rf final/form_8825/*.pdf final/form_8825/*.csv
	rm -rf final/form_schedule_e/*.pdf final/form_schedule_e/*.csv
	rm -rf final/form_unknown/*.pdf
	rm -rf intermediate/form_8825/*.jpg intermediate/form_schedule_e/*.jpg

define target_exists
	@(ls "final/form_8825/$(basename $(notdir $@))"*pdf > /dev/null 2>&1 || \
		ls "final/form_schedule_e/$(basename $(notdir $@))"*pdf > /dev/null 2>&1 || \
		ls "final/form_unknown/$(basename $(notdir $@))"*pdf > /dev/null 2>&1) && \
	echo 'Target $(notdir $@) has already been processed -- skipping'
endef

process/%:
	@echo "Processing $*"
	$(call target_exists) || python scripts/process_pdf.py "$*"
