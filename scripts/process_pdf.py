import sys
import shutil

import pytesseract
from pdf2image import convert_from_path

from helpers.textractor import Textractor
from helpers import utils

FORM_INDENTIFIERS = [
    ('8825', 'Rental Real Estate Income and Expenses'),
    ('schedule_e', 'Supplemental Income and Loss')
]


def get_form_type(text):
    """Check form for each identifier string and return first one found."""
    for key, value in FORM_INDENTIFIERS:
        if value in text:
            return key


def sort_document(file):
    """
    Convert PDF pages to images, then run local OCR to extract and sort pages.

    Adapted from
    https://www.geeksforgeeks.org/python-reading-contents-of-pdf-using-ocr-optical-character-recognition/
    """
    input_path = utils.get_raw_path(file)
    pages = convert_from_path(input_path, dpi=300, grayscale=True)

    forms = []
    form_idx = 1
    for page in pages:
        text = str(pytesseract.image_to_string(page))
        form_type = get_form_type(text)

        if form_type:
            intermediate_path = utils.get_intermediate_path(
                file,
                form_type,
                form_idx
            )
            page.save(
                intermediate_path,
                'JPEG'
            )
            forms.append((intermediate_path, form_type))
            form_idx += 1

    return forms


if __name__ == '__main__':
    file = sys.argv[1]
    forms = sort_document(file)

    if len(forms) == 0:
        print(
            f"No matching text found in {file}, copying to final/form_unknown"
        )
        input_path = utils.get_raw_path(file)
        final_pdf_path = utils.get_final_pdf_path(file, 'unknown')
        shutil.copy(input_path, final_pdf_path)
    else:
        for form_idx, (input_path, form_type) in enumerate(forms, start=1):
            textractor = Textractor(
                input_path,
                form_type,
                form_idx
            )
            blocks = textractor.get_blocks()
            textractor.extract_entity_list(blocks)
            textractor.redact_pdf(blocks)
