import glob
import os
import pandas as pd

# QA check if all the UUID has been processed (UUID came from each files usualy the first 4 blocks of digits connected with "-")

def extract_UUID(x):
   return  x.split("_")[0]

print("INTERITY CHECK: \n")
print("---------------- QA if enough files got processed: \n")
flist_sch_e = glob.glob("../../final/form_schedule_e/*.pdf")
flist_8825 = glob.glob("../../final/form_8825/*.pdf")
flist_unknown = glob.glob("../../final/form_unknown/*.pdf")
flist_raw = glob.glob("../../raw/*.pdf")

flist_unknown = list(map(lambda x: extract_UUID(x.split('\\')[-1]), flist_unknown ))
flist_8825 = list(map(lambda x: extract_UUID(x.split('\\')[-1]), flist_8825 ))
flist_sch_e = list(map(lambda x: extract_UUID(x.split('\\')[-1]), flist_sch_e ))
flist_raw = list(map(lambda x: extract_UUID(x.split('\\')[-1]), flist_raw ))

total_after = set(flist_sch_e) | set(flist_8825)  | set(flist_unknown)

non_processed = total_after ^ set(flist_raw)

if len(non_processed)/len(flist_raw) <= 0.05 :
    print("less than 5% non processed files: QA Passed")
else:
    print("more than 5% of files that did not get processed: QA Failed")

print("")

print("-------------- QA on Ouput file record counts: \n")
file_sch_e = open("final/form_schedule_e/entities.csv", 'r')
content = file_sch_e.readlines()
print("number of rows of scheule E entities.csv:")
print(len(content))
print(" ")
file_sch_e.close()

print("Number of Files in final/form_schedule_e:")
print(len(flist_sch_e))
print(" ")

file_8825 = open("final/form_8825/entities.csv", 'r')
content = file_8825.readlines()
print("number of rows of form 8825 entities.csv:")
print(len(content) )
print(" ")
file_sch_e.close()
file_8825.close()


print("Number of Files in final/form_8825:")
print(len(flist_8825))
print(" ")









