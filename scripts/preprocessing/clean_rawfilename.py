import glob
import os

flist = glob.glob("../..raw/original_raw_files/*.*")
print(flist)
print(len(flist))

# clean file from flst
def renamefile(f,path):
    formatted = f.split("\\")[-1].replace(' ', '').replace('PDF', 'pdf').replace("$", '').replace("#", '').replace("&", '_').replace(",", '').replace("%", '').replace(";", '')
    result = path +  '\\' + formatted
    return result

#rename files
for file in flist :
    new = renamefile(f = file, path = 'raw')
    os.rename(file,new)

newflist = glob.glob('raw/*.*')
print(len(newflist))

