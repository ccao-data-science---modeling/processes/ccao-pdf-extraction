import os
import pathlib


def get_raw_path(file):
    return os.path.join('raw', file)


def _get_basename(file, form_idx=None):
    stem = pathlib.Path(file).stem
    if form_idx is not None:
        basename = f'{stem}.{form_idx}'
    else:
        basename = stem
    return basename


def get_intermediate_path(file, form_type, form_idx=None):
    idx = form_idx if form_idx is not None and form_idx > 1 else None
    basename = _get_basename(file, idx)
    return os.path.join(
        'intermediate',
        f'form_{form_type}',
        f'{basename}.jpg'
    )


def get_final_csv_path(form_type):
    return os.path.join(
        'final',
        f'form_{form_type}',
        'entities.csv'
    )


def get_final_pdf_path(file, form_type, form_idx=None):
    basename = _get_basename(file, form_idx)
    return os.path.join(
        'final',
        f'form_{form_type}',
        f'{basename}.pdf'
    )
