import os
import csv

import boto3
from PIL import Image, ImageDraw

from . import constants
from helpers import utils


class Textractor:
    def __init__(self, input_path, form_type, form_idx=None):
        self.input_path = input_path
        self.filename = os.path.basename(self.input_path)
        self.form_type = form_type
        self.address_table_header = constants.ADDRESS_TABLE_HEADER
        self.form_idx = form_idx

        if self.form_type == '8825':
            self.fields = constants.FORM_8825_FIELDS
            self.income_table_header = constants.FORM_8825_INCOME_TABLE_HEADER
            self.address_table_dimensions = constants.FORM_8825_ADDRESS_TABLE_DIMENSIONS
        elif self.form_type == 'schedule_e':
            self.fields = constants.SCHEDULE_E_FIELDS
            self.income_table_header = constants.SCHEDULE_E_INCOME_TABLE_HEADER
            self.address_table_dimensions = constants.SCHEDULE_E_ADDRESS_TABLE_DIMENSIONS
        else:
            raise TypeError(
                f'Form type "{form_type} is not recognized, must be one of: 8825, schedule_e'
            )

        self.final_csv_path = utils.get_final_csv_path(form_type)
        self.final_pdf_path = utils.get_final_pdf_path(
            self.filename,
            form_type,
            form_idx=None  # Default to the idx in the input filename
        )

    def get_blocks(self):
        """
        Return AWS Textract blocks for forms and tables.
        """
        with open(self.input_path, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        client = boto3.client('textract')
        response = client.analyze_document(
            Document={'Bytes': bytes_test},
            FeatureTypes=['TABLES', 'FORMS']
        )

        return response['Blocks']

    def redact_pdf(self, blocks):
        """
        Given a list of Textract blocks, write a redacted PDF to the output file.

        Key-value block analysis code adapted from:
        https://docs.aws.amazon.com/textract/latest/dg/examples-extract-kvp.html
        """
        img = Image.open(self.input_path)
        draw = ImageDraw.Draw(img)
        img_width, img_height = img.size

        key_map = self._get_key_map(blocks)
        value_map = self._get_value_map(blocks)
        block_map = self._get_block_map(blocks)

        # Redact sensitive fields
        for block_id, key_block in key_map.items():
            key = self._get_text(key_block, block_map)
            for redacted_field in constants.REDACTED_FIELDS:
                if redacted_field in key:
                    # Get bounding box from the value block and redact it
                    value_block = self._find_value_block(key_block, value_map)
                    bbox = value_block['Geometry']['BoundingBox']
                    self._redact_bbox(draw, bbox, img_width, img_height)
                    break

        # Redact address table.
        # On our first attempt we tried doing this by using an address
        # classifier on all the text blocks, but the precision/recall wasn't
        # good enough. For now, use a more naive approach by detecting the
        # header of the table and blocking out its expected dimensions.
        addr_table_blocks = [
            block for block in blocks
            if self.address_table_header in block.get('Text', '')
        ]
        addr_table_block = addr_table_blocks[0] if len(addr_table_blocks) > 0 else None
        if addr_table_block:
            block_bbox = addr_table_block['Geometry']['BoundingBox']
            addr_table_bbox = {
                # Start at the bottom of the block (its top coord + height)
                'Top': block_bbox['Top'] + block_bbox['Height'],
                'Left': block_bbox['Left'],
                'Width': self.address_table_dimensions['Width'],
                'Height': self.address_table_dimensions['Height']
            }
            self._redact_bbox(draw, addr_table_bbox, img_width, img_height)

        img.save(self.final_pdf_path)

    def _redact_bbox(self, draw, bbox, img_width, img_height):
        """
        Redact a bounding box from a PIL ImageDraw object, given an img_width
        and img_height for offset.
        """
        # Bounding box coordinates are recorded as proportions of
        # total image width
        x1 = img_width * bbox['Left']
        y1 = img_height * bbox['Top']

        # Use the width of the bounding box to calculate the
        # right-hand x-coordinate
        box_width = img_width * bbox['Width']
        x2 = x1 + box_width

        # Same for the y-coordinate -- note that since Textract and
        # PIL set the y-axis origin at the top of the document, we
        # need to add y1 to the bounding box height, not subtract,
        # even though y1 represents the "top" of the box
        box_height = img_height * bbox['Height']
        y2 = y1 + box_height

        draw.rectangle(((x1, y1), (x2, y2)), fill='black')
        return draw

    def extract_entity_list(self, blocks):
        """
        Given a list of Textract blocks, extract the entity list and write
        it to a combined CSV.
        """
        income_table = self._get_income_table(blocks)
        try:
            income_header = next(income_table)
        except StopIteration:
            # No income table found, so skip this form
            income_header = {}

        # Use header row to determine the indexes of additional
        # properties
        col_to_idx = {}
        for idx, cell in income_header.items():
            if cell and self.income_table_header not in cell:
                col_to_idx[cell.strip().lower()] = idx

        # Adjust fieldnames to accomodate per-property values and sequences
        # where one field spans multiple rows
        fieldnames = ['File name', 'Form index']
        for field in self.fields.values():
            # Passing None to _format_property_field() will cause the argument
            # to be ignored, but we still want to iterate that argument, so
            # if the field is not property/sequence-specific then use a
            # list with one NoneType element
            sequence = field.get('sequence') or [None]
            columns = col_to_idx.keys() if field.get('multiple') is True else [None]
            for seq in sequence:
                for col in columns:
                    fieldnames.append(
                        self._format_property_field(field['label'], col, seq)
                    )

        file_exists = os.path.isfile(self.final_csv_path)

        with open(self.final_csv_path, 'a+') as output_file:
            writer = csv.DictWriter(
                output_file,
                fieldnames=fieldnames
            )

            # If the compiled CSV does not exist yet, write its header
            if not file_exists:
                writer.writeheader()

            # Initialize clean representation of the row
            row = {
                'File name': os.path.splitext(self.filename)[0],
                'Form index': self.form_idx
            }
            sequence = None  # The current row sequence
            seq_idx = None  # The current index in the sequence
            field = None  # The label of the current field
            columns = col_to_idx  # The columns of the table to iterate
            for row_data in income_table:
                if sequence is None:
                    # We are not yet in a sequence, so parse the row
                    row_label = row_data[1]
                    for raw_label, field_meta in self.fields.items():
                        if raw_label in row_label:
                            # Standardize fieldname
                            field = field_meta['label']

                            # Check if this field represents a sequence
                            sequence = field_meta.get('sequence')
                            seq_idx = sequence[0] if sequence else None

                            # Check if this is a per-property field
                            if field_meta['multiple'] is True:
                                columns = col_to_idx
                            else:
                                try:
                                    columns = {None: col_to_idx[field_meta['column'].lower()]}
                                except KeyError:
                                    # Textract didn't recognize the column, so
                                    # skip it
                                    columns = {}

                            break
                    else:
                        # This row label did not match any of the recorded
                        # fieldnames, so set columns to empty in order to skip
                        # writing the row
                        columns = {}

                for col, idx in columns.items():
                    prop_field = self._format_property_field(field, col, seq_idx)
                    row[prop_field] = row_data[idx].strip()

                # Check whether we should increment or exit the sequence
                if sequence is not None:
                    if seq_idx >= sequence[-1]:
                        sequence, seq_idx = None, None
                    else:
                        seq_idx += 1

            writer.writerow(row)

    def _format_property_field(self, field, col=None, seq=None):
        """
        Format a field for a specific property and/or sequence value, like
        'Expenses - Other 1 (property A)'
        """
        field_str = field
        if seq is not None:
            field_str += f' {seq}'
        if col is not None:
            field_str += f' (property {col.upper()})'
        return field_str

    def _get_income_table(self, blocks):
        """Given a list of Textract blocks, return the income data table"""
        table_blocks = self._filter_table_blocks(blocks)
        block_map = self._get_block_map(blocks)
        for table in table_blocks:
            row_column_map = self._get_row_column_map(table, block_map)
            # Skip empty tables (table row/cols are 1-indexed by Textract)
            if row_column_map.get(1):
                # The header value can sometimes be parsed in the first, second,
                # or third column, so handle each case appropriately
                for col_idx in (1, 2, 3):
                    if row_column_map[1].get(col_idx):
                        header_idx = col_idx
                        break
                else:
                    # This table is almost certainly not useful, since it does not
                    # have any data in the first, second, or third columns of the
                    # header row
                    continue
                # Only extract income table
                if self.income_table_header in row_column_map[1][header_idx]:
                    for row_data in row_column_map.values():
                        yield row_data

    def _get_block_map(self, blocks):
        """
        Given a list of Textract blocks, return a dictionary mapping block IDs
        to block data.
        """
        return {block['Id']: block for block in blocks}

    def _filter_table_blocks(self, blocks):
        """
        Given a list of Textract blocks, return blocks that represent table data.
        """
        return [block for block in blocks if block['BlockType'] == 'TABLE']

    def _filter_form_blocks(self, blocks):
        """
        Given a list of Textract blocks, return blocks that represent form data.
        """
        return [block for block in blocks if block['BlockType'] == 'KEY_VALUE_SET']

    def _get_key_map(self, blocks):
        """
        Given a list of Textract blocks that represent form data, return a key map
        representing form keys.
        """
        form_blocks = self._filter_form_blocks(blocks)
        return {
            block['Id']: block for block in form_blocks
            if 'KEY' in block['EntityTypes']
        }

    def _get_value_map(self, blocks):
        """
        Given a list of Textract blocks that represent form data, return a value map
        representing form values.
        """
        form_blocks = self._filter_form_blocks(blocks)
        return {
            block['Id']: block for block in form_blocks
            if 'VALUE' in block['EntityTypes']
        }

    def _find_value_block(self, key_block, value_map):
        for relationship in key_block['Relationships']:
            if relationship['Type'] == 'VALUE':
                for value_id in relationship['Ids']:
                    value_block = value_map[value_id]
        return value_block

    def _get_text(self, result, block_map):
        text = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = block_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                        if word['BlockType'] == 'SELECTION_ELEMENT':
                            if word['SelectionStatus'] == 'SELECTED':
                                text += 'X '
        return text

    def _get_row_column_map(self, result, block_map):
        """
        Given a table result and a map of blocks, return the rows and columns
        in that table as a dict.
        """
        rows = {}
        for relationship in result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = block_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            rows[row_index] = {}
                        rows[row_index][col_index] = self._get_text(cell, block_map)
        return rows
