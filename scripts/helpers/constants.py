# Map raw Textract fields to the matching output field
FORM_8825_FIELDS = {
    '2 Gross rents': {  # The raw key as displayed in OCRed forms
        'multiple': True,  # Whether or not this field can apply to multiple properties
        'label': 'Income - Gross rents',  # Clean display label for this field
    },
    'Rental Real Estate Expenses 3 Advertising': {
        'multiple': True,
        'label': 'Expenses - Advertising',
    },
    '4 Auto and travel': {
        'multiple': True,
        'label': 'Expenses - Auto and travel',
    },
    '5 Cleaning and maintenance': {
        'multiple': True,
        'label': 'Expenses - Cleaning and maintenance',
    },
    '6 Commissions': {
        'multiple': True,
        'label': 'Expenses - Commissions',
    },
    '7 Insurance': {
        'multiple': True,
        'label': 'Expenses - Insurance',
    },
    '8 Legal and other professional fees': {
        'multiple': True,
        'label': 'Expenses - Legal and other professional fees',
    },
    '9 Interest (see instructions)': {
        'multiple': True,
        'label': 'Expenses - Interest',
    },
    '10 Repairs': {
        'multiple': True,
        'label': 'Expenses - Repairs',
    },
    '11 Taxes': {
        'multiple': True,
        'label': 'Expenses - Taxes',
    },
    '12 Utilities': {
        'multiple': True,
        'label': 'Expenses - Utilities',
    },
    '13 Wages and salaries': {
        'multiple': True,
        'label': 'Expenses - Wages and salaries',
    },
    '14 Depreciation (see instructions)': {
        'multiple': True,
        'label': 'Expenses - Depreciation',
    },
    '15 Other (list)': {
        'multiple': True,
        'label': 'Expenses - Other',
        'sequence': [1, 2, 3],  # This field forms a sequence with the following two fields
    },
    '16 Total expenses for each property. Add lines 3 through 15': {
        'multiple': True,
        'label': 'Expenses - Total per property',
    },
    '17 Income or (loss) from each property. Subtract line 16 from line 2': {
        'multiple': True,
        'label': 'Income - Total per property',
    },
    '18a Total gross rents': {
        'multiple': False,
        'column': 'D',  # The property column that is used for this value
        'label': 'Income - Total gross rents',
    },
    'b Total expenses': {
        'multiple': False,
        'column': 'D',
        'label': 'Expenses - Total',
    },
    '19 Net gain (loss)': {
        'multiple': False,
        'column': 'D',
        'label': 'Net gain (loss) from disposition of property',
    },
    '20a Net income (loss)': {
        'multiple': False,
        'column': 'D',
        'label': 'Net income (loss) from rental real estate activities',
    },
    '21 Net rental real estate income (loss)': {
        'multiple': False,
        'column': 'D',
        'label': 'Net rental real estate income (loss)',
    },
}
FORM_8825_INCOME_TABLE_HEADER = 'Rental Real Estate Income'
FORM_8825_ADDRESS_TABLE_DIMENSIONS = {
    'Width': 0.41458823529411765,
    'Height': 0.24727272727272728,
}

SCHEDULE_E_FIELDS = {
    '3 Rents received': {
        'multiple': True,
        'label': 'Income - Rents received',
    },
    '4 Royalties received': {
        'multiple': True,
        'label': 'Income - Royalties received',
    },
    '5 Advertising': {
        'multiple': True,
        'label': 'Expenses - Advertising',
    },
    '6 Auto and travel': {
        'multiple': True,
        'label': 'Expenses - Auto and travel',
    },
    '7 Cleaning and maintenance': {
        'multiple': True,
        'label': 'Expenses - Cleaning and maintenance',
    },
    '8 Commissions': {
        'multiple': True,
        'label': 'Expenses - Commissions',
    },
    '9 Insurance': {
        'multiple': True,
        'label': 'Expenses - Insurance',
    },
    '10 Legal and other professional fees': {
        'multiple': True,
        'label': 'Expenses - Legal and other professional fees',
    },
    '11 Management fees': {
        'multiple': True,
        'label': 'Expenses - Management fees',
    },
    '12 Mortgage interest paid to banks': {
        'multiple': True,
        'label': 'Expenses - Mortgage interest paid to banks',
    },
    '13 Other interest': {
        'multiple': True,
        'label': 'Expenses - Other interest',
    },
    '14 Repairs': {
        'multiple': True,
        'label': 'Expenses - Repairs',
    },
    '15 Supplies': {
        'multiple': True,
        'label': 'Expenses - Supplies',
    },
    '16 Taxes': {
        'multiple': True,
        'label': 'Expenses - Taxes',
    },
    '17 Utilities': {
        'multiple': True,
        'label': 'Expenses - Utilities',
    },
    '18 Depreciation expense or depletion': {
        'multiple': True,
        'label': 'Expenses - Depreciation or depletion',
    },
    '19 Other (list)': {
        'multiple': True,
        'label': 'Expenses - Other',
    },
    '20 Total expenses': {
        'multiple': True,
        'label': 'Expenses - Total',
    },
    '21 Subtract line 20 from line 3 (rents)': {
        'multiple': True,
        'label': 'Income - Rents and royalties minus expenses',
    },
    '22 Deductible rental real estate loss': {
        'multiple': True,
        'label': 'Income - Deductable rental real estate loss after limitation',
    },
    '23a Total of all amounts reported on line 3': {
        'multiple': False,
        'column': 'B',
        'label': 'Income - Total rents received for all properties',
    },
    'b Total of all amounts reported on line 4': {
        'multiple': False,
        'column': 'B',
        'label': 'Income - Total royalties received for all properties',
    },
    'c Total of all amounts reported on line 12': {
        'multiple': False,
        'column': 'B',
        'label': 'Expenses - Total mortgage interest paid to banks for all properties',
    },
    'd Total of all amounts reported on line 18': {
        'multiple': False,
        'column': 'B',
        'label': 'Expenses - Total depreciation or depletion for all properties',
    },
    'e Total of all amounts reported on line 20': {
        'multiple': False,
        'column': 'B',
        'label': 'Expenses - Total for all properties',
    },
    '24 Income. Add positive amounts shown on line 21': {
        'multiple': False,
        'column': 'C',
        'label': 'Income - Total positive income',
    },
    '25 Losses. Add royalty losses from line 21': {
        'multiple': False,
        'column': 'C',
        'label': 'Expenses - Total losses',
    },
    '26 Total rental real estate and royalty income or (loss)': {
        'multiple': False,
        'column': 'C',
        'label': 'Total rental real estate and royalty income or loss',
    },
}
SCHEDULE_E_INCOME_TABLE_HEADER = 'Income:'
SCHEDULE_E_ADDRESS_TABLE_DIMENSIONS = {
    'Width': 0.8920619554695063,
    'Height': 0.043091655266757865
}

# Fields that must be redacted from PDFs
REDACTED_FIELDS = [
    'Name', 'Name(s) shown on return',
    'Employer identification number', '(2) Employer identification number', 'Employer Identification number',
    'Your soclal security number', 'Your social security number'
]

ADDRESS_TABLE_HEADER = 'Physical address of each property'
